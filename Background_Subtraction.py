#!/usr/bin/env python

__author__    = "Lisa Dang"
__copyright__ = "Copyright (C) 2017 Lisa Dang"
__license__   = "Public Domain"
__version__   = "1.0"

from astropy.io import fits
from astropy.stats import biweight_location

from photutils import make_source_mask
from photutils import Background2D, SigmaClip, MedianBackground

import numpy as numpy
import matplotlib.pyplot as pyplot

import glob
import os

def get_names(directory, ftype, ch):
	lst     = os.listdir(directory)
	lst     = lst[k for k in lst if ftype in k]
return lst

def estimate_bg(data):

	
	return median, std
