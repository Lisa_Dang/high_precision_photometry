#--------------------------------------------------------------
#Author: Lisa Dang
#Created: 2016-10-13 1:29 PM EST
#Last Modified: 2016 - 11 - 08 EST
#Title: Main Function for High Precision Photometry
#--------------------------------------------------------------

import numpy as np
import os, sys
import Photometry_Aperture as APhotometry
import Photometry_PSF as PSFPhotometry
import Photometry_Companion as CPhotometry
import Photometry_PLD as PLDPhotometry
import Photometry_Aperture_Routine as Routine

def create_folder(fname):
	solved = 'no'
	while(solved == 'no'):
		path = 	os.path.dirname(os.path.abspath(__file__)) + '/' + fname
		if not os.path.exists(path):
			os.makedirs(path)
			solved = 'yes'
		else :
			print('Error:', fname, 'already exist! Are you sure you want to overwrite this folder? (y/n)')
			answer = input()
			if (answer=='y'):
				solved = 'yes'
			else:
				print('What would you like the new folder name to be?')
				fname = input()
	return fname

def main():

	# General
	folder     = 'Run13'
	#datapath   = 'C:/Users/Lisa/Documents/Exoplanets/High_Precision_Photometry/Modified_Data/Oversampled'
	datapath   = 'C:/Users/Lisa/Dropbox/CoRoT-2b/Spitzer'
	#datapath   = 'C:/Users/Lisa/Desktop'
	
	
	AOR_snip   = 'r579'                                # first characters of AOR directories
	channel    = 'ch2'                                 # spitzer channel used
	subarray   = True    

	#photometry = 'Routine'                            # 
	photometry = 'Aperture'
	radius     = 2.5


	# create folder
	folder = create_folder(folder)
	savepath   = 'C:/Users/Lisa/Documents/Exoplanets/high_precision_photometry/' + folder
	
	# Do Everything
	# Frame by Frame Diagnostics
	#exec(open("outlier.py").read())
	# Photometry 
	if   (photometry == 'Aperture'):
		APhotometry.get_lightcurve(datapath, savepath, AOR_snip, channel, subarray)
	elif (photometry == 'PSFfit'):
		PSFPhotometry.get_lightcurve(datapath, savepath, AOR_snip, channel, subarray)
	elif (photometry == 'Companion'):
		CPhotometry.get_lightcurve(datapath, savepath, AOR_snip, channel, subarray, r = radius)
	elif (photometry == 'PLD'):
		PLDPhotometry.get_pixel_lightcurve(datapath, savepath, AOR_snip, channel, subarray)
	elif (photometry == 'Routine'):
		Routine.get_lightcurve(datapath, savepath, AOR_snip, channel, subarray, r = radius*2 + 0.5)

	else:
		print('Sorry,', photometry, 'is not supported by this pipeline!')
	# MCMC Fitting
	#exec(open("CoRot-2b_MCMC.py").read())
	#Make Report
	#exec(open("CoRot-2b_Report.py").read())

if __name__=='__main__': main()